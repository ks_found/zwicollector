#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Create a collection of ZWI files from the list of URL. 
#
import urllib,urllib3
import hashlib, requests
from datetime import datetime,timezone
import time
import shutil
import os,time,sys,io,gzip,os.path
import argparse
from urllib.parse import urlparse
# This package
from common import *

# external module
sys.path.append('ZWIBuilder')
from zwi_producer import *
from zwi_index import *

#### input parameters ####
kwargs = {}
parser = argparse.ArgumentParser()
parser.add_argument('-q', '--quiet', action='store_true', help="don't show verbose")
parser.add_argument("-i", '--input', help="Input file with URLs to articles (one URL per line)")
parser.add_argument("-o", '--output', help="Output directory for ZWI and Cache files")
parser.add_argument("-l", '--lang', help="2-character language codei (ISO 639‑1). ISO 639‑2 is used when 639‑1 is not available")

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


args = parser.parse_args()
isVerbose=True
if (args.quiet): isVerbose=False;

if len(sys.argv)==1:
    parser.print_help(sys.stderr)
    sys.exit(1)

print("Input file with URL list  =",args.input)
print("Output directory with ZWI files  =",args.output)
print("Is quiet =",args.quiet)

finput="publishers/wikipedia.txt"
if (args.input != None):
    if len(args.input)>0:
        finput=args.input

isExist = os.path.exists(args.output)
if not isExist:
   os.makedirs(args.output)
   print("The new directory ",args.output," is created!")

# extract language
Lang=args.lang

# output ZWI directory
zwiPath=args.output+"/ZWI/en"
isExist = os.path.exists(zwiPath)
if not isExist:
   os.makedirs(zwiPath)
   print("The new directory ", zwiPath," is created!")

# HTML cache directory
cachePath=args.output+"/cache/en/html"
if not os.path.exists(cachePath):
   os.makedirs(cachePath)
   print("The new directory ", cachePath," is created!")

file1 = open(finput, 'r')
Lines = file1.readlines()
count = 0

def sanitizeFileName(name):
    dangerousCharacters = [" ", '"', "'", "&", "/", "\\", "?", "*"];
    for c in dangerousCharacters:
          name=name.replace(c,"_")
    return name

import requests

def getParam(title):
    params = {
    'action': 'parse',
    'page': title,
    'redirects':'',
    'prop': 'text|wikitext',
    'format': 'json'
    }
    return params
   
# write cached files
def writeCacheFile(html,wiki,cached_dir,title):
    xfile=cached_dir+"/"+title+".html.gz" 
    with gzip.open(xfile, 'wb') as f:
       f.write(html.encode())
       xfile=cached_dir+"/"+title+".html.gz"
    xfile=cached_dir+"/"+title+".wikitext.gz"
    with gzip.open(xfile, 'wb') as f:
       f.write(wiki.encode())
       f.close()


# process..
# Strips the newline character
for line in Lines:
    count += 1
    line=line.strip()
    parsed = urlparse(line.strip())
    #print (parsed.hostname, parsed.path); # en.wikipedia.org   
    fpath=parsed.path
    if (fpath.startswith("/")):
                        fpath=fpath[1:len(fpath)]
    titles=(fpath).split("/")
    title=titles[len(titles)-1]
    ZWIname=title+".zwi"
    ZWIname=sanitizeFileName(ZWIname)
    # cache name
    CacheName=title.replace("/","#")
    CacheName=sanitizeFileName(CacheName)
    CacheName=CacheName+".html.gz"

    if (len(titles)==2):
                 ZWIname=titles[0]+"#"+ZWIname
    if (len(titles)==3):
                 ZWIname=titles[0]+"#"+titles[1]+"#"+ZWIname
    print(count,") Title=",title," ZWI=",ZWIname," Cache=",CacheName)

    sitepars=getPublisherFromURL(line.strip())
    #print(sitepars)
    params=getParam(title)
    #print(params)
    r = requests.get(sitepars["SITE_API"], params=params)
    #print(r)
    data = r.json()

    try:
      raw_html = data['parse']['text']['*']
      raw_wikitext = data['parse']['wikitext']['*']
    except KeyError:
       print("Error with parsing=",title)
       continue

    # filter articles based on 
    if (raw_wikitext.find("#REDIREC")>-1): 
                       print("Redirect for title=",title,"Continue")
                       continue
    if (len(raw_wikitext)<500): 
                        print("Too short article=",title,"Continue")
                        continue
    xmd5=hashlib.md5(title.encode()).hexdigest()
    dir1=xmd5[0:1]
    dir2=xmd5[0:2]
    source = sitepars["SITE_SHORT"]
    cached_dir=cachePath+"/"+source+"/"+dir1+"/"+dir2
    if not os.path.exists(cached_dir):
          os.makedirs(cached_dir)
    html_file=cached_dir+"/"+title+".html.gz"

    if os.path.exists(html_file):
                   print("Cached file for title=",title," exists. Continue") 
                   continue

    # optional sleep
    time.sleep(1)

    writeCacheFile(raw_html,raw_wikitext,cached_dir,title)
    # create ZWI file
    zwi_dir= zwiPath+"/"+source+"/"+parsed.hostname 
    if not os.path.exists(zwi_dir):
          os.makedirs(zwi_dir)
    zwi_file=zwi_dir+"/"+ZWIname
    ztitle=title
    print("Language=",Lang)
    makeZWI(html_file, zwi_file, ztitle, source, Lang)

# create new index
createIndexFiles(zwiPath, excluded=[])

print("")
print(f"{bcolors.OKBLUE}All done. Summary:{bcolors.ENDC}")
print("Nr of ZWI files: ",count)
print("ZWI directory  :",zwiPath)
print("Cache directory:",cachePath)
