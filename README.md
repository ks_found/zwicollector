# ZWICollector

A stand-alone package to create collections of ZWI files from a remote web site. The package is tuned to Mediawiki engines, but will be extended  to any arbitrary web site. 

[[_TOC_]]


## Workflow

- Step 0: Input:  A text file (*.txt) with URLs of articles of a publisher. Each title should on a new line. 


- Step 1:  The package will reads the URLs line by line. It will extract title.html and title.wikitext (if Mediawiki). The files should be copied to MD5-style directory for encycloreader's "caching" and compressed.  Example: The article with the title "Zuera" from ikipedia will be moved to the directory: 

 
```
output/cache/en/html/wikipedia/0/06/Zuera.gz  (this is the original HTML)
output/cache/en/html/wikipedia/0/06/Zuera.wikitext.gz (original wikitext)
```
Note "wikipedia" in the directory path is the publisher.

- Step 2: ZWIBuilder package processes Zuera.html and Zuera.wikitext to create the ZWI file. The ZWI file will be put to 

```      
output/ZWI/en/wikipedia/en.wikipedia.org/wiki#Zuera.zwi
```
according to the index convention (where en.wikipedia.org/wiki is made from URL). See the description in [ZWIBuilder](https://gitlab.com/ks_found/ZWIBuilder/)


- Step 3: Apply the zwi_signature command     

- Step 4: Then the program goes to (1) to process another line with the title

- Step 5: After all lines are processed it runs zwi_index to index the collection


Finally, all files will be published using [ZWINetwork](https://gitlab.com/ks_found/ZWINetwork). The cached directory will be copied to encycloreader (for self-update features of encycloreader). 

## Installation

First, install html2text ("apt install python3-html2textI' for Ubuntu). Then: 

```
git@gitlab.com:ks_found/zwicollector.git
cd zwicollector
git clone git@gitlab.com:ks_found/ZWIBuilder.git
```

## Usage

Here is an example of how to process Wikipedia articles:

```
python3 zwi_collect.py -l en -i publishers/wikipedia.txt -o output 
```

If you want to add a new encyclopedia, please modify the function "getPublisherFromURL" (in common.py) which includes the API.

## Support

## Authors and acknowledgment

## License

GNU General Public License. https://www.gnu.org/licenses/gpl-3.0.en.html

## Project status
