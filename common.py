# common module for all sites
# S.Chekanov
# get parameters of publishers from URL

def getPublisherFromURL(url):
    if (url.find("en.wikipedia.org")>-1):
        return {"SITE":"Wikipedia","SITE_API":"https://en.wikipedia.org/w/api.php","SITE_URL":"https://en.wikipedia.org/wiki/","SITE_SHORT":"wikipedia"}
    if (url.find("af.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_af","SITE_API":"https://af.wikipedia.org/w/api.php","SITE_URL":"https://af.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_af"}
    if (url.find("ar.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_ar","SITE_API":"https://ar.wikipedia.org/w/api.php","SITE_URL":"https://ar.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_ar"}
    if (url.find("bn.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_bn","SITE_API":"https://bn.wikipedia.org/w/api.php","SITE_URL":"https://bn.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_bn"}
    if (url.find("de.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_de","SITE_API":"https://de.wikipedia.org/w/api.php","SITE_URL":"https://de.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_de"}
    if (url.find("es.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_es","SITE_API":"https://es.wikipedia.org/w/api.php","SITE_URL":"https://es.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_es"}
    if (url.find("fr.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_fr","SITE_API":"https://fr.wikipedia.org/w/api.php","SITE_URL":"https://fr.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_fr"}
    if (url.find("hi.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_hi","SITE_API":"https://hi.wikipedia.org/w/api.php","SITE_URL":"https://hi.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_hi"}
    if (url.find("it.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_it","SITE_API":"https://it.wikipedia.org/w/api.php","SITE_URL":"https://it.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_it"}
    if (url.find("ja.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_ja","SITE_API":"https://ja.wikipedia.org/w/api.php","SITE_URL":"https://ja.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_ja"}
    if (url.find("pt.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_pt","SITE_API":"https://pt.wikipedia.org/w/api.php","SITE_URL":"https://pt.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_pt"}
    if (url.find("ru.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_ru","SITE_API":"https://ru.wikipedia.org/w/api.php","SITE_URL":"https://ru.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_ru"}
    if (url.find("zh.wikipedia.org")>-1):
        return {"SITE":"Wikipedia_zh","SITE_API":"https://zh.wikipedia.org/w/api.php","SITE_URL":"https://zh.wikipedia.org/wiki/","SITE_SHORT":"wikipedia_zh"}
    if (url.find("handwiki.org")>-1):
        return {"SITE":"Handwiki","SITE_API":"https://handwiki.org/wiki/api.php","SITE_URL":"https://handwiki.org/wiki/","SITE_SHORT":"handwiki"}
    if (url.find("citizendium")>-1):
        return {"SITE":"Citizendium","SITE_API":"https://citizendium.org/wiki/api.php","SITE_URL":"https://citizendium.org/wiki/","SITE_SHORT":"citizendium"}
    if (url.find("encyclopediaofmath")>-1):
        return {"SITE":"Encyclopedia of Mathematics","SITE_API":"https://encyclopediaofmath.org/api.php","SITE_URL":"https://encyclopediaofmath.org/wiki/","SITE_SHORT":"encyclopediaofmath"}

